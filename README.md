# [dc20e6.ru/research/apache-x0](https://dc20e6.ru/research/apache-x0)

> TL;DR; Apache из дефолтной конфигурации принимает все методы (Request method) и передает на сторону сервера приложения.

Описание исследования и эксплуатации: [https://dc20e6.ru/research/apache-x0](https://dc20e6.ru/research/apache-x0)

## The fix

В конфиг apache добавить

```
<LimitExcept GET POST HEAD>
    Order deny,allow
    Deny from all
</LimitExcept>
```